all:
	erlc -I /usr/lib/erlang/lib/xmerl-1.3.1/include -o ebin/ src/*.erl
	cp src/chat_bot.app ebin/chat_bot.app

clean:
	rm ebin/*