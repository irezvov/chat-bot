-module(chat_bot_feed).

-export([get_last_news/0, get_last_news/1]).

-define(URL, "http://news.yandex.ru/index.rss").

-define(NEWS_COUNT, 5).

-include_lib("xmerl.hrl").

get_last_news() ->
    get_last_news(?URL).

get_last_news(URL) ->
    { ok, { _Code, _Headers, Body } } = httpc:request(URL),
    { Doc, _Rest } = xmerl_scan:string(Body),
    Items = lists:sublist(xmerl_xpath:string("//item", Doc), ?NEWS_COUNT),
    lists:map(fun item_to_news/1, Items).

item_to_news(Item) ->
    [{ title, get_title_text(Item) }].

get_title_text(Item) ->
    [ Text ] = xmerl_xpath:string("title/text()", Item),
    xml_text_to_list(Text).

xml_text_to_list(#xmlText{ value = S }) ->
    S;
xml_text_to_list(_) ->
    "".
