-module(chat_bot_cache).

-behaviour(gen_server).

-export([ start_link/0, get_last_news/0 ]).

-export([ init/1, handle_call/3, handle_cast/2, handle_info/2,
	  terminate/2, code_change/3 ]).

-define(CACHE_TIME, 60 * 1000).

-record(state, { cache = [], expired = infinity, update_time = 0 }).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

get_last_news() ->
    gen_server:call(?MODULE, get_last).

init([]) ->
    { ok, #state{ } }.

handle_call(get_last, _From, #state{ cache = [] }) ->
    News = chat_bot_feed:get_last_news(),
    NewState = #state{ cache = News, expired = ?CACHE_TIME, update_time = now_ms() },
    { reply, News, NewState, ?CACHE_TIME };
handle_call(get_last, _From, #state{ cache = News } = State) ->
    NewState = time_least(State),
    { reply, News, NewState, NewState#state.expired }.

handle_cast(_, State) ->
    NewState = time_least(State),
    { noreply, NewState, NewState#state.expired }.

handle_info(timeout, _State) ->
    { noreply, #state{} };
handle_info(_, State) ->
    NewState = time_least(State),
    { noreply, NewState, NewState#state.expired }.

terminate(_Reason, _State) ->
    ok.

code_change(_Old, State, _Extra) ->
    { ok, State }.

now_ms() ->
    {MegaSecs,Secs,_MicroSecs} = now(),
    (MegaSecs*1000000 + Secs)*1000.

time_least(#state{ cache = [] } = State) ->
    State;
time_least(#state{ expired = E, update_time = U } = State) ->
    S = now_ms(),
    State#state{ expired = E - (S - U), update_time = S }.
