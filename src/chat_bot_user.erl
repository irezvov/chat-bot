-module(chat_bot_user).

-behaviour(gen_server).

-export([ start_link/0 ]).

-export([ init/1, handle_call/3, handle_cast/2, handle_info/2,
	  terminate/2, code_change/3 ]).

-define(NICK, "News").

-record(state, {}).

start_link() ->
    gen_server:start_link(?MODULE, [], []).

init([]) ->
    chat_manager:login(?NICK),
    { ok, #state{} }.

handle_call(_, _From, State) ->
    { noreply, State }.

handle_cast({ send, { private_from, Login, Command } }, State) ->
    handle_command(Login, trim(Command)),
    { noreply, State };
handle_cast({ send, _ }, State) ->
    { noreply, State }.

handle_info(_, State) ->
    { noreply, State }.

terminate(_Reason, _State) ->
    chat_manager:remove_client(),
    ok.

code_change(_Old, State, _Extra) ->
    { ok, State }.

get_last_news() ->
    News = chat_bot_cache:get_last_news(),
    string:join(lists:map(fun news_to_string/1, News), "\n").

news_to_string(News) ->
    {title, Title} = proplists:lookup(title, News),
    Title.

trim(Str) ->
    lists:filter(fun($\n) -> false;
		    ($\r) -> false;
		    (_) -> true end, Str).

handle_command(Login, "latest") ->
    News = get_last_news(),
    chat_manager:private(Login, unicode:characters_to_binary(News));
handle_command(Login, Command) ->
    chat_manager:private(Login, "Unknown command: " ++ Command ++ "\n").
