{ application, chat_bot,
  [{ description, "News bot for chat server" },
   { vsn, "1.0.0" },
   { modules, [
     chat_bot_app,
     chat_bot_sup,
     chat_bot_feed,
     chat_bot_cache,
     chat_bot_user
   ]},
   { registered, [chat_bot_sup] },
   { application, [kernel, stdlib, inets, chat] },
   { mod, { chat_bot_app, [] } }
  ]
}.