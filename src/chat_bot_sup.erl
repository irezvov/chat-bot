-module(chat_bot_sup).

-behaviour(supervisor).

-export([ start_link/0 ]).

-export([ init/1 ]).

start_link() ->
    supervisor:start_link({ local, ?MODULE }, ?MODULE, []).

init([]) ->
    Children = [
		{ chat_bot_user, { chat_bot_user, start_link, [] },
		  permanent, 2000, worker, [chat_bot_user] },
		{ chat_bot_cache, { chat_bot_cache, start_link, [] },
		  permanent, 2000, worker, [chat_bot_cache] }
	       ],
    Strategy = { one_for_one, 0, 1 },
    { ok, { Strategy, Children } }.
